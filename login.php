<?php
	
require_once("../system/init.php");

$user = new \pongsit\user\user();
$role = new \pongsit\role\role();

$notification = '';

// add view
$variables = array();
$variables['h1'] = $view->block('h1',array('message'=>'เข้าสู่ระบบ','css'=>'col-sm-10 col-md-8 col-lg-6 col-xl-4 text-center'));
$variables['hide-main-menu'] = 'hide';
$variables['notification'] = $notification;
$variables['sign_up_hide']='hide';
$variables['social-login'] = $view->block('social-login');

echo $view->create($variables);
