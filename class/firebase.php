<?php

namespace pongsit\firebase;

class firebase extends \pongsit\model\model{
	public function __construct(){
		parent::__construct();
	}
	function get_the_info($userId){
		$query = "SELECT * FROM ".$this->table." where userId='$userId';";
		$outputs = $this->db->query_array0($query);
		return $outputs;
	}
	function get_user_id($firebase_id){
		$query = "SELECT user_id FROM firebase_user where firebase_id='$firebase_id';";
		$outputs = $this->db->query_array0($query);
		if(!empty($outputs['user_id'])){
			return $outputs['user_id'];
		}else{
			return false;
		}
	}
	function count_me(){
		$results = $this->db->query_array0("select count(distinct userId) as count from ".$this->table);
		return $results['count'];
	}
	function search_for_user($s){
		$query = "SELECT * FROM ".$this->table." WHERE displayName like '%$s%' LIMIT 10;";
		$outputs = $this->db->query_array($query);
		return $outputs;
	}
	function get_info_from_user_id($user_id){
		$query = "SELECT firebase_id FROM firebase_user where user_id='$user_id';";
		$outputs = $this->db->query_array0($query);
		if(!empty($outputs['firebase_id'])){
			return $this->get_info($outputs['firebase_id']);
		}else{
			return array();
		}
	}
	function insert_user($firebase_id,$user_id){
		return $this->db->insert('firebase_user',array('user_id'=>$user_id,'firebase_id'=>$firebase_id));
	}
	function login($userId){
		$auth = new \pongsit\auth\auth();
		$user = new \pongsit\user\user();
		$url = new \pongsit\url\url();
		$firebase_user_infos = $this->get_the_info($userId);
		$user_id = '';
		if(!empty($firebase_user_infos['id'])){
			$user_id = $this->get_user_id($firebase_user_infos['id']);
		}
		if(empty($user_id)){
			return false;
		}
		$user_infos = $user->get_info($user_id);
		if(!$user_infos['active']){
			if($user_id!=1){
				return false;
			}
		}
		$urls = $url->get_current('array');
		$auth->add_session(array('user','id'),$user_infos['id']);
		$auth->add_session(array('user','username'),$user_infos['name']);
		$auth->add_session(array('user','name'),$user_infos['name']);
		$auth->add_session(array('user','baseurl'), $GLOBALS['baseurl']);
		
		if(!empty($firebase_user_infos['pictureUrl']) && !file_exists($GLOBALS['path_to_app'].'system/img/profile/'.$user_id)){
			copy($firebase_user_infos['pictureUrl'], $GLOBALS['path_to_app'].'system/img/profile/'.$user_id);
		}
		
		return true;	
	}
}



// CREATE TABLE `firebase` (
//   `id` int(11) NOT NULL,
//   `userId` text NOT NULL,
//   `displayName` text,
//   `pictureUrl` text,
//   `status` int(11) DEFAULT '1'
// ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

// --
// -- Indexes for dumped tables
// --

// --
// -- Indexes for table `firebase`
// --
// ALTER TABLE `firebase`
//   ADD PRIMARY KEY (`id`);

// --
// -- AUTO_INCREMENT for dumped tables
// --

// --
// -- AUTO_INCREMENT for table `firebase`
// --
// ALTER TABLE `firebase`
//   MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


// CREATE TABLE `firebase_user` (
//   `id` int(11) NOT NULL,
//   `user_id` int(11) NOT NULL,
//   `firebase_id` int(11) NOT NULL
// ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

// --
// -- Indexes for dumped tables
// --

// --
// -- Indexes for table `firebase_user`
// --
// ALTER TABLE `firebase_user`
//   ADD PRIMARY KEY (`id`);

// --
// -- AUTO_INCREMENT for dumped tables
// --

// --
// -- AUTO_INCREMENT for table `firebase_user`
// --
// ALTER TABLE `firebase_user`
//   MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;