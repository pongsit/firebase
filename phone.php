<?php
	
require_once("../system/init.php");

if(empty($confVariables['firebase'])){
	// require
	// $confVariables['firebase']
	echo 'Please set up the config for firebase login. See the code for more detail.';
	exit();
}

$user = new \pongsit\user\user();
$role = new \pongsit\role\role();

$notification = '';

// add view
$variables = array();
$variables['h1'] = $view->block('h1',array('message'=>'เข้าสู่ระบบด้วยเบอร์โทรศัพท์','css'=>'col-sm-10 col-md-8 col-lg-6 col-xl-4 text-center sign-in-card'));
$variables['hide-main-menu'] = 'hide';
$variables['notification'] = $notification;
// เปลี่ยน header เฉพาะหน้านี้
// $variables['header'] = $view->block('header');
$variables['sign_up_hide']='hide';
// if($invitation_infos['active']==1){
// 	$variables['sign_up_hide']='';
// }

echo $view->create($variables);
