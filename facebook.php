<?php
	
require_once("../system/init.php");

if(empty($confVariables['firebase'])){
	// require
	// $confVariables['firebase']
	echo 'Please set up the config for firebase login. See the code for more detail.';
	exit();
}

$variables['go'] = '';

// if(empty($_SESSION['gotofacebook'])){
// 	$variables['go'] = 'firebase.auth().signInWithRedirect(provider);';
// 	$_SESSION['gotofacebook'] = 1;
// }else{
// 	$variables['go'] = '';
// 	unset($_SESSION['gotofacebook']);
// }

echo $view->create($variables);
