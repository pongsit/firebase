Best practice for Firebase login with redirect

refer to the officail doc here

https://firebase.google.com/docs/auth/web/redirect-best-practices#update-authdomain

Recommend Option 4

Placing the file in this path

(project root)/__/auth/(filename)

make folder name => handler
make folder name => iframe
move ==> wget https://<project>.firebaseapp.com/__/auth/handler to index.html under handler folder
move ==> wget https://<project>.firebaseapp.com/__/auth/handler.js under handler folder
move ==> wget https://<project>.firebaseapp.com/__/auth/experiments.js under handler folder
move ==> wget https://<project>.firebaseapp.com/__/auth/iframe to index.html under iframe folder
move ==> wget https://<project>.firebaseapp.com/__/auth/iframe.js under iframe folder

change authDomain in firebaseConfig to your domain