<?php
	
require_once("../system/init.php");

$firebase = new \pongsit\firebase\firebase();

use Firebase\JWT\JWT;
use Firebase\JWT\Key;

if(empty($_POST['token'])){
	exit();
}

$id_token = $_POST['token'];

// Retrieve the public keys from Google
$json = file_get_contents("https://www.googleapis.com/robot/v1/metadata/x509/securetoken@system.gserviceaccount.com");
$keys = json_decode($json, true);

// Choose the appropriate key for decoding based on the kid (key ID) from the JWT header
$header = JWT::jsonDecode(JWT::urlsafeB64Decode(explode('.', $id_token)[0]), true);
$kid = $header->kid;
$key = $keys[$kid];

$decoded = JWT::decode($id_token, new Key($key, 'RS256'));

if(empty($decoded)){
	exit();
}

$userId = $decoded->user_id;
if(!empty($decoded->email)){
	$displayName = $decoded->email;
}else if(!empty($decoded->phone_number)){
	$displayName = $decoded->phone_number;
}

$user_infos = $firebase->get_the_info($userId);
$user_info_preps = array('userId'=>$userId);
if(!empty($displayName)){
	$user_info_preps['displayName'] = $displayName;
}
if(!empty($decoded->picture)){
	$user_info_preps['pictureUrl'] = $decoded->picture;
}

if(empty($user_infos)){
	$firebase->insert_once($user_info_preps,'userId',$userId);
	header('Location: '.$path_to_core.'firebase/user-insert.php?userId='.$userId);
	exit();
}else{
	$firebase->update($user_info_preps,' userId="'.$userId.'"');
	if($firebase->login($userId)){
		$redirect = 'index.php';
		if(!empty($_SESSION['user']['id'])){
			$redirect = $path_to_core.'firebase/user-info.php?id='.$_SESSION['user']['id'];
		}
		header('Location: '.$redirect);
		exit();
	}else{
		header('Location: '.$path_to_core.'firebase/user-insert.php?userId='.$userId);
		exit();
	}
}



