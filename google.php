<?php
	
require_once("../system/init.php");

if(empty($confVariables['firebase'])){
	// require
	// $confVariables['firebase']
	echo 'Please set up the config for firebase login. See the code for more detail.';
	exit();
}

$variables['go'] = '';

// $variables['redirect'] = $host.'/redirect.php';

// if(empty($_SESSION['gotogoogle'])){
// 	$variables['go'] = 'signInWithRedirect(auth, provider);';
// 	$_SESSION['gotogoogle'] = 1;
// }else{
// 	$variables['go'] = '0';
// 	unset($_SESSION['gotogoogle']);
// }

echo $view->create($variables);
