<?php
	
require_once("../system/init.php");

// $option = new \pongsit\view\option();
$role = new \pongsit\role\role();
$user = new \pongsit\user\user();
$firebase = new \pongsit\firebase\firebase();

if(empty($_GET['userId'])){
	$view = new \pongsit\view\view('warning');
	echo $view->create();
	exit();
}

$firebase_user_infos = $firebase->get_the_info($_GET['userId']);
if(empty($firebase_user_infos['id'])){
	$view = new \pongsit\view\view('warning');
	echo $view->create();
	exit();
}
$firebase_id = $firebase_user_infos['id'];
$displayName = $firebase_user_infos['displayName'];

if(!empty($firebase->get_user_id($firebase_id))){
	if($firebase->login($_GET['userId'])){
		header('Location: '.$path_to_core.'firebase/user-info.php?id='.$_SESSION['user']['id']);
		exit();
	}else{
		$view = new \pongsit\view\view('locked');
		echo $view->create();
		exit();
	}
}

$notification = '';
if(!empty($_POST)){
	if( !empty($_POST['username']) && 
		!empty($_POST['password']) &&
		!empty($_POST['password_retype']) &&
		!empty($firebase_id)
	){
		unset($_POST['submit']);
		if($_POST['password']==$_POST['password_retype']){
		}else{
			$notification = $view->block('alert',array('message'=>'Password 2 ครั้งไม่ตรงกันครับ','type'=>'danger','css'=>'col-md-7'));
		}
		$username = $_POST['username'];
		$password = $_POST['password'];
		if(!$user->exist($username)){
			$this_user_id = $firebase->get_user_id($firebase_id);
			if(!file_exists($path_to_root.'app/system/img/profile')){
				mkdir($path_to_root.'app/system/img/profile',0744,true);
			}
			if(empty($this_user_id)){
				$hash = $auth->generatePassword($password);
				$user_id = $user->insert(array('name'=>$username,'password'=>$hash));
				$firebase->insert_user($firebase_id,$user_id);
				if(!(@$_SESSION['user']['id']==1 || $role->check('admin'))){
					if($firebase->login($_GET['userId'])){
						// $view->create(array('user_id'=>$user_id));
						header('Location: '.$path_to_core.'firebase/user-info.php?id='.$user_id);
						exit();
					}else{
						$view = new \pongsit\view\view('message');
						$variables = array();
						$variables['message'] = 'มีบางอย่างผิดพลาด';
						echo $view->create($variables);
						error_log('firebase signup failed: firebase/user-insert.php');
						exit();
					}
				}
			}else{
				header('Location: '.$path_to_core.'firebase/user-info.php?id='.$this_user_id);
				exit();
			}
		}else{
			$notification = $view->block('alert',array('message'=>'มีผู้ใช้ชื่อ '.$username.' อยู่ในระบบแล้ว หากคุณคือเจ้าของ กรุณา <a href="'.$path_to_core.'auth/login.php">Log In</a> หากไม่ใช่กรุณาเลือก Username ใหม่ครับ','type'=>'danger','css'=>'col'));
		}
	}
}else{
	$username = $displayName;
	$password = $_GET['userId'];
	if(!$user->exist($username)){
		$this_user_id = $firebase->get_user_id($firebase_id);
		if(!file_exists($path_to_root.'app/system/img/profile')){
			mkdir($path_to_root.'app/system/img/profile',0744,true);
		}
		if(empty($this_user_id)){
			$hash = $auth->generatePassword($password);
			$user_id = $user->insert(array('name'=>$username,'password'=>$hash));
			$firebase->insert_user($firebase_id,$user_id);
			if(!(@$_SESSION['user']['id']==1 || $role->check('admin'))){
				if($firebase->login($_GET['userId'])){
					// $view->create(array('user_id'=>$user_id));
					header('Location: '.$path_to_core.'firebase/user-info.php?id='.$user_id);
					exit();
				}else{
					$view = new \pongsit\view\view('message');
					$variables = array();
					$variables['message'] = 'มีบางอย่างผิดพลาด';
					echo $view->create($variables);
					error_log('firebase signup failed: firebase/user-insert.php');
					exit();
				}
			}
		}else{
			header('Location: '.$path_to_core.'firebase/user-info.php?id='.$this_user_id);
			exit();
		}
	}else{
		$notification = $view->block('alert',array('message'=>'มีผู้ใช้ชื่อ '.$username.' อยู่ในระบบแล้ว หากคุณคือเจ้าของ กรุณา <a href="'.$path_to_core.'auth/login.php">Log In</a> หากไม่ใช่กรุณาเลือก Username ใหม่ครับ','type'=>'danger','css'=>'col'));
	}
}

$variables['notification'] = $notification;
// $variables['h1']=$view->block('h1',array('message'=>'กรุณาสร้าง Username ที่ต้องการ','css'=>'col-md-7 text-center'));
// $role_ids = array();
// if(!empty($_SESSION['user']['id'])){
// 	$role_ids = $role->get_all_role_id($_SESSION['user']['id'],$_SESSION['team']['id']);
// }
// 
// $max_role = $role_ids[0]['role_id'];
// $skips = array();
// for($i=$max_role-1;$i>0;$i--){
// 	$skips[$i] = $i;
// }
// $variables['role_option'] = $option->from_db('role','last',$skips);
$variables['profile-image']=$firebase_user_infos['pictureUrl'];
$variables['profile-displayName']=$firebase_user_infos['displayName'];
$variables['header'] = $view->block('header');
$variables['page-name'] = 'สร้าง Username';
echo $view->create($variables);

